+++
weight = 2100
title = "Frequently Asked Questions"
nameInMenu = "FAQ"
draft = false
+++

## When is SystemRescue going to support ZFS
ZFS will not be included because of its licence. If it was just a technical matter
it would already be included. The legal stuff is a gray area. Some people say it could
be distributed, some other people say it cannot. Hence we are not going to take any
legal risk.

However, we do provide a [script](/scripts/build-zfs-srm/) that makes it easy to build
a [SystemRescueModule (SRM)](/Modules/) containing the ZFS kernel module and tools.
This can be loaded into a stock SystemRescue session after booting. The SRM can also
be included directly in a custom-built SystemRescue ISO using the [customization
script](/scripts/sysrescue-customize/).

## Why is SystemRescue not able to automatically mount file systems
Most regular desktop distributions are able to automatically mount file systems,
including from removable devices, in order to make the access to your data easy.
but SystemRescue does not auto mount file systems in such a way, and there is a
good reason for it. SystemRescue can be used to perform physical copies of disks
or partitions at a block level using tools such as `dd`. It is essential that
file systems on these devices are not mounted when this happens. If file systems
were mounted automatically, these could cause these file systems to be accessed
unintentionally while such operations are being performed, which is harmful.
If data on a device are accessed by both the file system and by a tool such as `dd`,
it is very likely to cause corruptions on the device, or on the copy of the device.

See the [mountall script](/scripts/mountall/) for a convenient alternative to auto
mounting.

## Is there a 32-Bit release of SystemRescue?
SystemRescue offered a 32-Bit / i686 architecture release that was based on 
[Arch Linux 32](https://archlinux32.org/) from versions 6.0.7 to 9.03. Because the 
vast majority of Linux distributions now only support the 64-Bit AMD/Intel architecture,
the code for 32-Bit Intel architecture gets much less testing and development. This regularly 
lead to issues and continued support would have cost significant developement time.
Also nearly all CPUs sold since 2006 support 64-Bit code. This is why SystemRescue decided to
better invest the time in new features instead, see 
[Issue #278](https://gitlab.com/systemrescue/systemrescue-sources/-/issues/278) for details.

You can still download the old 32-Bit releases, see the "previous versions" section 
on the [Download page](/Download/).
