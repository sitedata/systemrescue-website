+++
title = "Start programs when the graphical desktop comes up: gui_autostart"
draft = false
+++

## Overview
SystemRescue contains a X.Org graphical desktop that can be started either by using the `dostartx` or `dovnc` [boot options](/manual/Booting_SystemRescue/) or running `startx` on the command line.
You can automatically start programs as soon as the graphical desktop is run.

## Configuration

Configuration for this is done solely through entries in the [YAML configuration](/manual/Configuring_SystemRescue/)
using the `gui_autostart` scope. The keys in the map / dictionary (100 and 200 in the example) are names for each program. 
The names are used for the `.desktop` files that are created to implement this:
```
---
gui_autostart:
    100:
        desktop: "/usr/share/applications/thunar.desktop"
    200:
        exec: "bash -c \"read -p 'press any key'\""
        terminal: true
```

## desktop
If there already is a `.desktop` file (see below) for the program you want to start, you can use the
`desktop` keyword to point to this file.

Using the `desktop` keyword is mutually exclusive with the `exec` keyword.

## exec
If there is no `.desktop` file for your program or it is missing things like the correct arguments, you can
use the `exec` keyword to execute an arbitrary command. The content is taken as-is and written to the
`Exec` key of the generated `.desktop` file. See the [documentation of the Exec key](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#exec-variables) for details.

When using `exec` you can also set `terminal` to let the program run in a terminal window
(`Terminal` option in the `.desktop` file).

Using the `exec` keyword is mutually exclusive with the `desktop` keyword.

## Implementation and relevant standards
The feature is implemented by creating .dektop files in the ~/.config/autostart directory,
as described in:
  * [Overview in the Arch Wiki](https://wiki.archlinux.org/title/Desktop_entries)
  * [Desktop Application Autostart Specification](https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html)
  * [Desktop Entry Specification](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html)
